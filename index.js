// Lesson 3
function fizzBuzz() {
  for (let i = 1; i <= 100; i++) {
    if (i % 15 === 0) {
      console.log("FizzBuzz");
    } else if (i % 5 === 0) {
      console.log("Buzz");
    } else if (i % 3 === 0) {
      console.log("Fizz");
    } else {
      console.log(i);
    }
  }
}

function filterArray(a) {
  let b = a.reduce((acc, el) => {
    if (Array.isArray(el)) {
      acc = [...acc, ...el];
    }
    return acc;
  }, []);
  let uniq = [...new Set(b)].sort((a, b) => a - b);
  return uniq;
}

// Output DON'T TOUCH!
// fizzBuzz();
console.log(
  filterArray([
    [2],
    23,
    "dance",
    [87],
    true,
    [3, 5, 3],
    [3442, 2878, 8778, 1],
    [4],
  ]),
);
